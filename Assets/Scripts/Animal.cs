using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpringJoint))]
public class Animal : MonoBehaviour
{
    public List<Wound> Wounds;

    private void Start()
    {
        foreach (var wound in Wounds)
        {
            wound.onClick += ONClick;
            wound.onDestroyed += ONDestroyed;
        }
    }

    public void PickUp()
    {
        foreach (var wound in Wounds)
        {
            wound.activated = true;
        }
    }

    public void LetGo()
    {
        foreach (Wound wound in Wounds)
        {
            wound.activated = false;
        }
    }

    private void ONDestroyed(object sender, Wound e)
    {
        e.onClick -= ONClick;
        e.onDestroyed -= ONDestroyed;
        
        Wounds.Remove(e);
        Destroy(e);
    }

    private void ONClick(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }
}