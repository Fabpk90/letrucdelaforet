using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AnimalPart : MonoBehaviour
{
    public Animal animal;
}