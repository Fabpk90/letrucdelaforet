using System;
using UnityEditor;
using UnityEngine;

public class RopeGenerator : MonoBehaviour
{
    public float Distance;
    public static float PickUpDistance;

    public GameObject prefab;
    public GameObject[] ropeParts;

    private int ropePartsUsed = 0;

    private GameObject _animalRef;
    private void Start()
    {
        PickUpDistance = Distance * 10;
        ropeParts = new GameObject[Mathf.CeilToInt(PickUpDistance)];
    }

    private void Update()
    {

    }

    /// <summary>
    /// This is not really working, the scale might be for some reason, check with more space between links
    /// </summary>
    /// <param name="worldSpacePosition"></param>
    public void CreateRope(Vector3 worldSpacePosition, Vector3 normal, GameObject objectToAttachTo)
    {
        Vector3 end = worldSpacePosition;
        Vector3 start = transform.position;
        Vector3 ropeDir = (end - start);
        end += normal; // this should be of the size of the capsule
        int numOfParts = Mathf.CeilToInt((start - end).magnitude);
        
        
        print(worldSpacePosition);
        
        print("Parts of the rope " + numOfParts);

        for (int i = 0; i < ropePartsUsed; i++)
        {
            Destroy(ropeParts[i]);
        }

        for (int i = 0; i < numOfParts; i++)
        {
            ropeParts[i] = Instantiate(prefab, Vector3.Lerp(start, end, i / ((float)numOfParts - 1)), Quaternion.identity, transform);
            ropeParts[i].transform.up = ropeDir.normalized;
        }
        
        for (int i = 0; i < numOfParts; i++)
        {
            var joint = ropeParts[i].GetComponent<CharacterJoint>();

            joint.connectedAnchor = ropeParts[i].transform.rotation * (joint.connectedAnchor);
            
            if (i == 0)
            {
                joint.connectedBody = ropeParts[i + 1].GetComponent<Rigidbody>();
                ropeParts[i].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            }
            else if (i == numOfParts - 1) // snapping first and last
            {
                joint.connectedBody = ropeParts[i - 1].GetComponent<Rigidbody>();
                ropeParts[i].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                ropeParts[i].transform.parent = objectToAttachTo.transform;
            }
            else
            {
                joint.connectedBody = ropeParts[i - 1].GetComponent<Rigidbody>();
            }
            //print(start + (ropeDir * i));
        }

        ropePartsUsed = numOfParts;

       // Time.timeScale = 0;
    }
}