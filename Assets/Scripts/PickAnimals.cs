using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;

public class PickAnimals : MonoBehaviour
{
    [InlineEditor]
    public RopeGenerator Rope;

    private Ray _ray;

    public Vector3 positionHit;
    private void Update()
    {
        if (Mouse.current.leftButton.wasPressedThisFrame)
        {
            print("Left Click Mouse");
            var transform1 = transform;
            _ray = new Ray(transform1.position, transform1.forward * RopeGenerator.PickUpDistance);
            
            print(RopeGenerator.PickUpDistance);
            
            Debug.DrawRay(_ray.origin, _ray.direction * RopeGenerator.PickUpDistance, Color.red, 5.0f);

            var hits = Physics.RaycastAll(_ray);

            foreach (RaycastHit raycastHit in hits)
            {
                print("hit");
                var animal = raycastHit.transform.gameObject.GetComponent<Animal>();

                if (animal)
                {
                    print("animal");
                    var joint = animal.GetComponent<SpringJoint>();
                    joint.connectedBody = Rope.GetComponent<Rigidbody>();
                    joint.connectedAnchor = Rope.transform.InverseTransformPoint(Rope.transform.position);

                    positionHit = raycastHit.point;
                    Rope.CreateRope(raycastHit.point, raycastHit.normal, animal.gameObject);
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(positionHit, 0.15f);
    }
}