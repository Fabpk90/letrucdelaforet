using System;
using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

/// <summary>
/// This does: activate ui for accepting heal, change camera settings, activate rotator, activate clickables
/// </summary>
public class MiniGameClick : MonoBehaviour
{
    public bool started = false;
    public Animal animal;
    public FirstPersonController controller;
    public GameObject gameplayOutside;
    public Transform cameraRoot;
    
    private Transform _previousCameraLocation;
    
    public void ActivateUi()
    {
        
    }

    public void DeActivateUi()
    {
        
    }

    private void Update()
    {
        if (started)
        {
            if (Mouse.current.leftButton.wasPressedThisFrame)
            {
                RaycastHit info;
                Physics.Raycast(Camera.current.ScreenPointToRay(Input.mousePosition), out info);

                Wound wound = info.transform.gameObject.GetComponent<Wound>();

                if (wound)
                {
                    wound.OnTouch();
                }
            }
        }
    }

    public void StartGame()
    {
        print("MiniGame Started !");
        ActivateUi();
        //change camera settings
        gameplayOutside.SetActive(false);
        controller.enabled = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;

        _previousCameraLocation = cameraRoot;

        animal.GetComponent<Rigidbody>().isKinematic = true;

        var transform1 = animal.transform;
        transform1.parent.position = transform.position;
        
        transform1.localRotation = Quaternion.identity;
        transform1.localPosition = new Vector3(0, 0.5f, 0);
        
        var miniGameTransform = _previousCameraLocation;
        
        miniGameTransform.LookAt(transform1);

        cameraRoot = miniGameTransform;

        //activate rotator
        animal.GetComponentInParent<ObjectRotator>().enabled = true;
        animal.GetComponentInParent<ObjectRotator>().isPickedUp = true;
        //activate clickables
        
        animal.PickUp();
    }

    public void ExitGame()
    {
        DeActivateUi();
        animal.GetComponent<Rigidbody>().isKinematic = false;
        cameraRoot = _previousCameraLocation;
        gameplayOutside.SetActive(true);
        controller.enabled = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        
        //let go of the animal, bye bye !
        animal.GetComponentInParent<ObjectRotator>().enabled = false;
        animal.GetComponentInParent<ObjectRotator>().isPickedUp = false;
        
        animal.LetGo();
    }
}
