using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Table : MonoBehaviour
{
    private Animal _animal;
    public MiniGameClick MiniGameClick;

    private void Update()
    {
        if ( !MiniGameClick.started && _animal && Input.GetKeyDown(KeyCode.E))
        {
            MiniGameClick.animal = _animal;
            MiniGameClick.StartGame();
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        print("touched !");
        var animal = other.gameObject.GetComponent<Animal>();

        if (animal)
        {
            print("yes !");

            _animal = animal;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        print("untouched !");
        var animal = other.gameObject.GetComponent<Animal>();

        if (animal)
        {
            _animal = null;
        }
    }
}
