using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class ObjectRotator : MonoBehaviour
{
    public float RotationSpeed;

    public Transform RotationAnchor;

    public bool isPickedUp;

    private bool mouseWasUp = false;
    
    private Vector3 lastMousePos;
    private void Start()
    {
        if (!RotationAnchor)
            RotationAnchor = transform;

        lastMousePos = new Vector3(Screen.width / 2.0f, Screen.height / 2.0f, 0);
    }

    private void Update()
    {
        if (!isPickedUp) return;
        
        if (Mouse.current.leftButton.wasReleasedThisFrame)
        {
            mouseWasUp = true;
        }
        else if (Mouse.current.leftButton.isPressed)
        {
            if (mouseWasUp)
            {
                lastMousePos = Input.mousePosition;
                mouseWasUp = false;
            }
            
            var mousePos = Input.mousePosition;
            var posDelta = (mousePos - lastMousePos);
            
            print("Delta " + posDelta);

            (posDelta.x, posDelta.y) = (posDelta.y, -posDelta.x);

            var mouseDir = posDelta.normalized;
            var mouseSpeed = posDelta.magnitude;

            transform.RotateAround(RotationAnchor.position, mouseDir, mouseSpeed * RotationSpeed);
            
            lastMousePos = mousePos;
        }
    }
}