using System;
using UnityEngine;

public class Wound : MonoBehaviour
{
    public bool activated = false;
    public uint numberOfTimeToClick = 0;
    private uint numberOfTimeClicked = 0;

    public EventHandler onClick;
    public EventHandler<Wound> onDestroyed;

    public void OnTouch()
    {
        if (activated)
        {
            onClick.Invoke(this, EventArgs.Empty);

            numberOfTimeClicked++;
            if (numberOfTimeClicked >= numberOfTimeToClick)
            {
                onDestroyed.Invoke(this, this);
            }
        }
    }
}